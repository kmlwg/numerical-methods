#include<iostream>
#include<cmath>

using namespace std;

double f(const double& x){
    return cos(x) - x;
}

double false_pos(double& x_l, double& x_r, const double& tol){
    double x_0 = 0;
    double x_1 = x_0;
    int i = 0;
    do {
        x_0 = (x_l * f(x_r) - x_r * f(x_l)) / (f(x_r) - f(x_l));

        if     (f(x_0) * f(x_r) < 0) x_l = x_0;
        else if(f(x_0) * f(x_l) < 0) x_r = x_0;
        else break;
        
        x_1 = (x_l * f(x_r) - x_r * f(x_l)) / (f(x_r) - f(x_l));
        cout << i << endl;
        i++;
    } while(abs((x_1 - x_0) / x_1) > tol);

    return x_0;
}

int main(){
    cout.precision(8);
    double x_l{0}, x_r{1}, tol{0.000000001};
    cout << false_pos(x_l, x_r, tol) << endl;
}